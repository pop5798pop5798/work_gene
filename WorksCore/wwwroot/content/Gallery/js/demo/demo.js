/*
 * blueimp Gallery Demo JS
 * https://github.com/blueimp/Gallery
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global blueimp, $ */

$(function() {
  'use strict'

  // Load demo images from flickr:
  $.ajax({
    url: 'https://api.flickr.com/services/rest/',
    data: {
      format: 'json',
      method: 'flickr.interestingness.getList',
      // eslint-disable-next-line camelcase
      api_key: '7617adae70159d09ba78cfec73c13be3'
    },
    dataType: 'jsonp',
    jsonp: 'jsoncallback'
  }).done(function(result) {
    var carouselLinks = []
    var linksContainer = $('#links')
    var baseUrl
    // Add the demo images as links with thumbnails to the page:
    $.each(result.photos.photo, function(index, photo) {
      baseUrl =
        'https://farm' +
        photo.farm +
        '.static.flickr.com/' +
        photo.server +
        '/' +
        photo.id +
        '_' +
        photo.secret
      $('<a/>')
        .append($('<img>').prop('src', baseUrl + '_s.jpg'))
        .prop('href', baseUrl + '_b.jpg')
        .prop('title', photo.title)
        .attr('data-gallery', '')
        .appendTo(linksContainer)
      carouselLinks.push({
        href: baseUrl + '_c.jpg',
        title: photo.title
      })
    })
    // Initialize the Gallery as image carousel:
    // eslint-disable-next-line new-cap
    blueimp.Gallery(carouselLinks, {
      container: '#blueimp-image-carousel',
      carousel: true
    })
  })

  // Initialize the Gallery as video carousel:
  // eslint-disable-next-line new-cap
  blueimp.Gallery(
    [
      {
        title: '開始遊戲',
        href: '/Content/works/rat-1.png',
        type: 'image/jpeg'
      },
      {
        title: '遊戲說明',
        href: '/Content/works/rat-2.png',
        type: 'image/jpeg'
      },
      {
         title: '分數獎勵',
        href: '/Content/works/rat-3.png',
        type: 'image/jpeg'
      },
      {
         title: '遊玩畫面',
        href: '/Content/works/rat.png',
        type: 'image/jpeg'
      },
      {
         title: '遊戲結算',
        href: '/Content/works/rat-4.png',
        type: 'image/jpeg'
      }
    ],
    {
      container: '#blueimp-video-carousel',
      carousel: true
    }
  )
  
  blueimp.Gallery(
    [
      {
        title: '開始遊戲',
        href: '/Content/works/rat-1.png',
        type: 'image/jpeg'
      },
      {
        title: '遊戲說明',
        href: '/Content/works/rat-2.png',
        type: 'image/jpeg'
      },
      {
         title: '分數獎勵',
        href: '/Content/works/rat-3.png',
        type: 'image/jpeg'
      },
      {
         title: '遊玩畫面',
        href: '/Content/works/rat.png',
        type: 'image/jpeg'
      },
      {
         title: '遊戲結算',
        href: '/Content/works/rat-4.png',
        type: 'image/jpeg'
      }
    ],
    {
      container: '#blueimp-brick-carousel',
      carousel: true
    }
  )
})
